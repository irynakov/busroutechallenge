package com.bus.route.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class FileReader {

    private static final Logger log = LoggerFactory.getLogger(FileReader.class);

    public Stream<String> getFileAsStream(String fileName) {
        Stream<String> lines = null;
        String path = null;
        try {
            path = new ClassPathResource(fileName).getFile().getCanonicalPath();
            lines = Files.lines(Paths.get(path));
        } catch (IOException e) {
            log.error(String.format("Error has occurred while reading the file %s", path));
            e.printStackTrace();
        }
        return lines;
    }
}
