package com.bus.route.service;

import com.bus.route.config.AppProperies;
import com.bus.route.model.BusRouteDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Service
public class BusRouteService {

    private static final Logger log = LoggerFactory.getLogger(BusRouteService.class);

    private static final int ROUTE_POINTS_SIZE = 2;
    private static final int SKIP_ID_INDEX = 1;
    @Autowired
    private FileReader fileReader;
    @Autowired
    private AppProperies properies;

    private Map<Integer, Integer> route = new HashMap<>();

    public BusRouteDto determineBusRoute(int departureID, int arrivalID) {
        log.debug(String.format("Determining if the route is direct (%s, %s)...", departureID, arrivalID));

        BusRouteDto dto = new BusRouteDto(departureID, arrivalID, false);
        Stream<String> stream = fileReader.getFileAsStream(properies.getFileName());
        if (stream != null) {
            boolean direct = stream.skip(SKIP_ID_INDEX).anyMatch(line -> {
                List<Integer> stationIDs = getStationIdsList(line);
                route = stationIDs.stream()
                        .filter(p -> p == departureID || p == arrivalID)
                        .collect(toMap(Function.identity(), stationIDs::indexOf));
                return route.size() == ROUTE_POINTS_SIZE && isDirect(departureID, arrivalID);

            });

            dto.setDirectRoute(direct);
            route.clear();
            log.debug(String.format("Route is direct: %s", direct));
        }

        return dto;
    }

    private boolean isDirect(int departureID, int arrivalID) {
        return route.get(departureID) != null && route.get(arrivalID) != null && (route.get(departureID) < route.get(arrivalID));
    }

    private List<Integer> getStationIdsList(String element) {
        return Arrays.asList(element.split(" "))
                .stream()
                .skip(SKIP_ID_INDEX)
                .map(Integer::valueOf)
                .collect(toList());
    }


}
