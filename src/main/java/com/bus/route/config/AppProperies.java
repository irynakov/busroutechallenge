package com.bus.route.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:config.properties")
public class AppProperies {

    @Value("${file.bus.route.name}")
    private String fileName;

    public String getFileName() {
        return fileName;
    }
}
