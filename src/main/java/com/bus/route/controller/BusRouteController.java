package com.bus.route.controller;

import com.bus.route.model.BusRouteDto;
import com.bus.route.service.BusRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class BusRouteController {

    @Autowired
    private BusRouteService service;

    @RequestMapping(value = "direct")
    public ResponseEntity<BusRouteDto> checkRouteIsDirect(@RequestParam(value = "dep_sid") int departureID, @RequestParam(value = "arr_sid") int arrivalID) {
        BusRouteDto dto = service.determineBusRoute(departureID, arrivalID);
        return ResponseEntity.ok(dto);
    }
}
