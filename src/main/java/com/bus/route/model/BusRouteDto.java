package com.bus.route.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusRouteDto {

    @JsonProperty("dep_sid")
    private int departureStationID;
    @JsonProperty("arr_sid")
    private int arrivalStationID;
    @JsonProperty("direct_bus_route")
    private boolean directRoute;

    public BusRouteDto(int departureID, int arrivalID, boolean direct) {
        this.departureStationID = departureID;
        this.arrivalStationID = arrivalID;
        this.directRoute = direct;
    }

    public int getDepartureStationID() {
        return departureStationID;
    }

    public void setDepartureStationID(int departureStationID) {
        this.departureStationID = departureStationID;
    }

    public int getArrivalStationID() {
        return arrivalStationID;
    }

    public void setArrivalStationID(int arrivalStationID) {
        this.arrivalStationID = arrivalStationID;
    }

    public boolean isDirectRoute() {
        return directRoute;
    }

    public void setDirectRoute(boolean directRoute) {
        this.directRoute = directRoute;
    }

    @Override
    public String toString() {
        return "BusRouteDto{" +
                "departureStationID=" + departureStationID +
                ", arrivalStationID=" + arrivalStationID +
                ", directRoute=" + directRoute +
                '}';
    }

}
