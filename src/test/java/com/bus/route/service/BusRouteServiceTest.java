package com.bus.route.service;

import com.bus.route.config.AppProperies;
import com.bus.route.model.BusRouteDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BusRouteServiceTest {

    @Mock
    FileReader fileReaderMock;
    @Mock
    private AppProperies properiesMock;

    @InjectMocks
    private BusRouteService service;

    private List<String> testData = Arrays.asList("10",
            "1 153 150 148 106 17 20 160 140 24",
            "2 5 142 106 11",
            "19 153 121 114 150 5",
            "13 153 148 169 106 11 12",
            "14 114 150 142 12 179 174 17",
            "6 5 138 148 12 174 118 16 19 184");


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        when(properiesMock.getFileName()).thenReturn("test.txt");
        when(fileReaderMock.getFileAsStream("test.txt")).thenReturn(testData.stream());
    }

    @Test
    public void testDetermineBusRouteDirect() {
        BusRouteDto busRouteDto = service.determineBusRoute(150, 140);
        Assert.assertTrue(busRouteDto.isDirectRoute());

    }

    @Test
    public void testDetermineBusRouteNotDirect() {
        BusRouteDto busRouteDto = service.determineBusRoute(160, 20);
        Assert.assertFalse(busRouteDto.isDirectRoute());
    }

    @Test
    public void testDetermineBusRouteNotDirectSameID() {
        BusRouteDto busRouteDto = service.determineBusRoute(20, 20);
        Assert.assertFalse(busRouteDto.isDirectRoute());
    }


}
